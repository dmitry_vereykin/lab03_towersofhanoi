/**
 * Created by Dmitry Vereykin on 9/24/2015.
 * With assistance from: Howard Fulmer, Heather Myers
 */

public class Towers {
    int numberRings;
    ArrayStack<Integer> peg1;
    ArrayStack<Integer> peg2;
    ArrayStack<Integer> peg3;

    public Towers(int n) {
        numberRings = n;
        peg1 = new ArrayStack<Integer>();
        peg2 = new ArrayStack<Integer>();
        peg3 = new ArrayStack<Integer>();

        for (int i = n; i > 0; i--) {
            peg1.push(i);
        }
    }

    public int countRings(int pegNumber){
        int count = 0;

        if (pegNumber == 1) {
            count = peg1.size();
        } else if (pegNumber == 2) {
            count = peg2.size();
        } else if (pegNumber == 3) {
            count = peg3.size();
        }

        return count;
    }

    public int getTopDiameter(int pegNumber) {
        int topDiam = 0;

        if (countRings(pegNumber) > -1) {
            if (pegNumber == 1)
                topDiam = peg1.top();
            if (pegNumber == 2)
                topDiam = peg2.top();
            if (pegNumber == 3)
                topDiam = peg3.top();
        } else {
            topDiam = 0;
        }

        return topDiam;
    }

    public boolean validMove(int startPeg, int endPeg) {
        if ((startPeg >= 1 && startPeg <= 3 && endPeg >= 1 && endPeg <= 3 && startPeg != endPeg && countRings(startPeg) > -1)
                && ((countRings(endPeg) == -1) || getTopDiameter(startPeg) < getTopDiameter(endPeg)))
            return true;
        else
            return false;
    }

    public void move(int startPeg, int endPeg) {

        if (startPeg == 1 && endPeg == 2) {
            peg2.push(peg1.top());
            peg1.pop();
        }

        if (startPeg == 1 && endPeg == 3) {
            peg3.push(peg1.top());
            peg1.pop();
        }

        if (startPeg == 2 && endPeg == 1) {
            peg1.push(peg2.top());
            peg2.pop();
        }

        if (startPeg == 2 && endPeg == 3) {
            peg3.push(peg2.top());
            peg2.pop();
        }

        if (startPeg == 3 && endPeg == 1) {
            peg1.push(peg3.top());
            peg3.pop();
        }

        if (startPeg == 3 && endPeg == 2) {
            peg2.push(peg3.top());
            peg3.pop();
        }
    }

    public boolean victoryCondition() {
        if (peg1.isEmpty() && peg2.isEmpty()) {
            System.out.println("VICTORY!");
            return true;
        } else {
            return false;
        }
    }

    //I think there is an easier way to print, but it was the best that I could come up with at 4am
    public void printPegs() {
        String rings1;
        String rings2;
        String rings3;
        String s = "";

        System.out.print("------------------\n");

        for (int i = numberRings - 1; i >= 0; i--) {
            rings1 = peg1.toString(i);
            rings2 = peg2.toString(i);
            rings3 = peg3.toString(i);


            if (peg1.toString(i).equals("null")) {
                rings1 = " ";
            }

            if (peg2.toString(i).equals("null")) {
                rings2 = " ";
            }

            if (peg3.toString(i).equals("null")) {
                rings3 = " ";
            }
            System.out.print("  " + rings1 + "  |  " + rings2 + "   |  " + rings3 + "\n");
        }

        System.out.print("------------------\n");
        System.out.print("Peg1 | Peg2 | Peg3");
        System.out.println();
    }


}
